""" Settings for Redbeard """

# Connection settings
server = 'your.server.here'  # Server hostname/ip
port = 6697  # Port to connect to
channels = ("#yourchannel1","yourchannel2")  # Channel to connect to. Only one channel can be used currently
nick = 'redbeard'  # Name the bot should use

# Admin for the bot (Beware security risks on networks without service/daemon secured nicknames!)
owner = 'yournick'

# Name of log file to use. You can include a path here as well.
logfile = 'redbeard.log'

# Default language modifier to use (Pirate, Smurf, Normal)
language = 'pirate'

# Modules
module_list = (
    'example',   # Example module framework for custom plugins
    'jester',    # Tells dirty and clean jokes on request
    'fortunes',  # Gives a random fortune
    'quotes',    # Gives a random quote
    'bofh',      # Gives a random BOFH-style excuse
    )

# Enable/Disable SSL
ssl_enable = True
