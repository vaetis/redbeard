#!/usr/bin/python

import socket, os, ssl, sys, datetime

# Bot settings
server = 'irc.shadow.agency'
port = 6697
channels = ("#lab","#freeipa")
nick = 'friday'
owner = 'vaetis'

# Command list
commands = ( 	
			"!help",
			"!info",
			"!shutdown"
			)

# Functions
def ping():
	ircsock.send('PONG :ping\n')
	
def speak(channel, msg):
    if len(msg) > 490: 
        output = [msg[i:i+490] for i in range(0,len(msg), 490)] 
        for message_chunk in output: 
            ircsock.send('PRIVMSG '+ channel +' :'+ message_chunk +'\n')
            sleep(2) 
    else:
        ircsock.send('PRIVMSG '+ channel +' :'+ msg +'\n')

def joinchan(channels):
	for channel in channels:
		ircsock.send('JOIN '+ channel +'\n')
def process_message(irc_rawdata):
	global user
	global hostmask
	global channel
	global content
	info_list = irc_rawdata.split(' ')
	user_host = info_list[0]
	user_host = user_host.split('!')
	user = user_host[0].replace(':','')
	hostmask = user_host[1].split('@')
	hostmask = hostmask[1]
	channel = info_list[2]
	content = irc_rawdata.split(' :')
	content = str(content[1])

# A quick and easy way to check for core bot commands
def command_check(data):
	for command in commands:
		if command in content and user == owner:
			if command == "!help":
				speak(channel, "I am a basic bot framework.")
				speak(channel, "My functions are:")
				speak(channel, "!help: Print this output")
				speak(channel, "!info: Print basic bot information")
				speak(channel, "!shutdown: Shutdown the bot if you are the owner")
			# Check for info request
			elif command == "!info":
				speak(channel, "My localtime: " + datetime.datetime.now().strftime("%A, %d. %B %Y %I:%M%p"))
			# Check for shutdown command (not terrifically secure if someone has your name)
			elif command == "!shutdown":
				speak(channel, "Shutting down...")
				quit()

# Function for initiating a connection and providing a socket to send and recieve info through
# This creates a global socket which may allow a module to send directly and bypass all bot 
# functions, and should be more cleanly implemented.
def connect():
	try:
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.connect((server, port))
	except:
		print "Unable to connect."
	global ircsock
	ircsock = ssl.wrap_socket(sock)
	ircsock.send('USER '+ nick +' '+ nick +' '+ nick +' :Tagline!\n')
	ircsock.send('NICK '+ nick +'\n')
	joinchan(channels)
	return ircsock

def main():
	pid = os.fork()
	if pid > 0:
		sys.exit(0)
	os.setsid()
	print "Loaded. Connecting to "+ server +" and becoming a daemon..."
	ircsock = connect()
	while True:
		irc_rawdata = ircsock.recv(2048) # Set a buffer for data obtained by the server
		irc_rawdata = irc_rawdata.strip('\n\r') # Clear unwanted linebreaks in the raw data
		if irc_rawdata.find("PING :") != -1:
			ping()
		elif 'PRIVMSG' in irc_rawdata:
			process_message(irc_rawdata)
			command_check(irc_rawdata)

if __name__ == "__main__":
	main()