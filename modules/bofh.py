#!/usr/bin/python -B

import os, random
from time import sleep

### BOFH Excuse Generator  Module ###

# Keywords
keywords = (
	"!excuse",
	"what is it this time"
)

# Functions

def get_excuse():
    path = os.path.dirname(os.path.realpath(__file__))
    location = path + '/db/excuses.txt'
    with open(location) as lines:
        excuses = lines.readlines()
    random.shuffle(excuses, random.random)
    excuse = random.choice(excuses)
    return excuse

### Main output function ###
def return_data(language, speak, nick, user, hostmask, channel, content):
	for item in keywords:
		if item in content.lower().replace('?', '') and nick in content.replace(':', '') or item in content and content[:1] == '!':
                    ## Your code here
                    data = get_excuse()
                    speak(channel, language, data)
