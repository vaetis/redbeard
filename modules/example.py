#!/usr/bin/python -B

import os, random
from time import sleep

### Example Module ###

# Keywords
keywords = (
	"!example",
	"example keyword string"
)

### Main output function ###
def return_data(language, speak, nick, user, hostmask, channel, content):
	for item in keywords:
		if item in content.lower().replace('?', '') and nick in content.replace(':', '') or item in content and content[:1] == '!':
			## Your code here
			# Example sending directly to the channel provided:
			data = "my string"
			speak(channel, language, data)
	# Return output to the channel here; delete this if you don't need to return any strings when the module is finished
	return (content)
