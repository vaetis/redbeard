#!/usr/bin/python -B

import os, random
from time import sleep

### Example Module ###

# Keywords
keywords = (
	"!fortune",
)

# Functions
# Functions
def get_fortune():
	path = os.path.dirname(os.path.realpath(__file__))
	location = path + '/db/fortunes.txt'
	with open(location) as lines:
		fortunes = lines.readlines()
	random.shuffle(fortunes, random.random)
	fortune = random.choice(fortunes)
	return (fortune)

### Main output function ###
def return_data(language, speak, nick, user, hostmask, channel, content):
	for item in keywords:
		if item in content.lower().replace('?', '') and nick in content.replace(':', '') or item in content and content[:1] == '!':
			fortune = get_fortune()
			speak(channel, "Normal", fortune)
	# Return output to the channel here; delete this if you don't need to return any strings when the module is finished
	return (content)
