#!/usr/bin/python -B

import os, random
from time import sleep
from subprocess import call

### Module for telling jokes ###

# Keywords
keywords = (
	'a joke',
	'a dirty joke',
	'another joke',
	'another dirty joke',
	'!joke',
	'!dirtyjoke',
	'translate to pirate',
	'translate to smurf'
)

def get_joke(type):
	if type == 'clean':
		path = os.path.dirname(os.path.realpath(__file__))
		location = path + '/db/jokes.txt'
	elif type == 'dirty':
		path = os.path.dirname(os.path.realpath(__file__))
		location = path + '/db/dirtyjokes.txt'
	with open(location) as lines:
		jokes = lines.readlines()
	random.shuffle(jokes, random.random)
	joke = random.choice(jokes)
	return (joke)

### Main output function ###
def return_data(language, speak, nick, user, hostmask, channel, content):
	for item in keywords:
		if item in content.lower().replace('?', '') and nick in content.replace(':', '') or item in content and content[:1] == '!':
			# Collection of random pirate-isms
			if 'dirtyjoke' in content or 'dirty joke' in content:
				joke_type = 'dirty'
			else:
				joke_type = 'clean'

			# Colorful follow-ups to jokes
			endings = [
				'Arrr Harrr Harrr!',
				'Harrr!',
				'Harr Harr!',
				'Arrrr!',
				'By my majestic piratey beard!',
				'Run a shot across teh bow of her poopdeck, arrrr!',
				'Ye scurvy dog!',
				'Ye salty sea dog!',
				'By Davey Jones\' locker!',
				'By the crack in Ol\' Jenny\'s teacup!',
				'Fire in the hole!',
				'Give no quarter! Nor tree fiddy!',
				'Shivver me timber!',
				'Yo-ho-hoooo!',
				'By the jig of a hempen hoop!',
				'And scupper yer self!',
				'And weigh me anchor arrr!',
				'Dead men can\'t say no!',
				'Ye festering grog blossom!',
				'Loaded to me gunwalls, I be!',
				'Splice the mainbrace and swing me lead!'
			]
			end = random.shuffle(endings)
			end = random.choice(endings)

			# Fetch a joke from the collection(s)
			joke = get_joke(joke_type)

			# If the joke has a punch line
			if 'A: ' in joke:
				joke = joke.split(' A: ')
				question = joke[0]
				answer = 'A: ' + joke[1]
				speak(channel, language, question)
				sleep(4)
				speak(channel, language, answer)
				sleep(1)
				if language == "pirate":
					if random.randrange(1, 10) > 5:
						speak(channel, language, end)

			# If the joke is a one-liner
			else:
				speak(channel, language, joke)
				sleep(1)
				if language == "pirate":
					if random.randrange(1, 10) > 7:
						speak(channel, language, end)

	# Return output to channel here
	return (content)