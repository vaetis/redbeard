#!/usr/bin/python -B

import os, random
from time import sleep

### Example Module ###

# Keywords
keywords = (
	"!quote",
)

# Functions
def get_quote():
	path = os.path.dirname(os.path.realpath(__file__))
	location = path + '/db/quotes.txt'
	with open(location) as lines:
		quotes = lines.readlines()
	random.shuffle(quotes, random.random)
	quote = random.choice(quotes)
	return (quote)

### Main output function ###
def return_data(language, speak, nick, user, hostmask, channel, content):
	for item in keywords:
		if item in content.lower().replace('?', '') and nick in content.replace(':', '') or item in content and content[:1] == '!':
			quote = get_quote()
			speak(channel, "Normal", quote)
	# Return output to the channel here; delete this if you don't need to return any strings when the module is finished
	return (content)
