#!/usr/bin/python -B

import socket, os, ssl, sys, datetime, imp, random
from time import sleep
from conf import settings

## Default Command List
# Note: With the exception of the Core Module, individual module commands are not included here as they are intrinsically separate from the
# main script.
commands = (
	'!help',
	'!info',
	'!joke',
	'!dirtyjoke',
	'!translate smurf',
	'!translate pirate',
	'!shutdown'
)

## Import Settings
server = settings.server
channels = settings.channels
port = settings.port
nick = settings.nick
owner = settings.owner
language = settings.language
module_list = settings.module_list
ssl_enabled = settings.ssl_enable

## Debug Mode
debug = 0

## Functions

# Process input through loaded modules.
def check_modules(nick, user, hostmask, channel, content):
	for module in module_list:
		loaded_module = imp.load_source(module, './modules/' + module + '.py')
		getreply = getattr(loaded_module, 'return_data')
		output = getreply(language, speak, nick, user, hostmask, channel, content)
		if output != content and output != None:
			speak(channel, output)

# Send regular keep-alive pings so the server doesn't kill the connection as a time-out.
def ping():
	ircsock.send('PONG :ping\n')
	if debug == 1:
		print 'Keep-alive Ping sent.'

# Process output through here. Some modules may bypass this method local to themselves using the ircsock global.
def speak(channel, language, msg):
	output = []
	msg = str(msg)
	if language == 'pirate':
		msg = pirate(msg)
	if language == 'smurf':
		msg = smurf(msg)
	if debug == 1:
		print msg

	if len(msg) > 450:
		for i in range(0, len(msg), 450):
			output.append(msg[i:i + 450])
		for message_chunk in output:
			ircsock.send('PRIVMSG ' + channel + ' :' + message_chunk + '\n')
			sleep(2)
	else:
		ircsock.send('PRIVMSG ' + channel + ' :' + msg + '\n')

# Pirate Translation
def pirate(content):
	content = content.replace('You are ', 'Yer ')
	content = content.replace(' you are ', ' ye be ')
	content = content.replace(' here ', ' about ')
	content = content.replace('Hello', 'Yo-ho-ho')
	content = content.replace(' hello', ' yo-ho-ho')
	content = content.replace('Hey ', 'Yo-ho-ho')
	content = content.replace(' hey ', ' yo-ho-ho')
	content = content.replace('Your ', 'Yer ')
	content = content.replace(' your ', ' yer ')
	content = content.replace(' you\'re ', ' yer ')
	content = content.replace(' they ', ' thems ')
	content = content.replace('They ', 'Them ')
	content = content.replace('Yes ', 'Yar ')
	content = content.replace(' yes ', ' yar ')
	content = content.replace(' friend', ' matey')
	content = content.replace('This is ', 'This\'ere ')
	content = content.replace('This ', 'This ere ')
	content = content.replace('treasure', 'booty')
	content = content.replace('everyone', 'me hardies')
	content = content.replace('Everyone', 'Me hardies')
	content = content.replace(' is ', ' be ')
	content = content.replace(' you ', ' yer ')
	content = content.replace(' the ', ' teh ')
	content = content.replace('The ', 'Teh ')
	content = content.replace(' have ', ' be havin ')
	content = content.replace(' ass ', ' bung ')
	content = content.replace(' fucking ', ' wench spoilin ')
	content = content.replace(' bitch ', ' wench ')
	content = content.replace(' Fan ', ' Saltiest of Wenches ')
	content = content.replace(' my ', ' me ')
	content = content.replace('You ', 'Ye ')
	content = content.replace(' you ', ' ye ')
	content = content.replace('Your ', 'Yer ')
	content = content.replace(' was ', ' \'twere ')
	content = content.replace('people ', 'landlubbers ')
	content = content.replace('People ', 'Landlubbers ')
	content = content.replace('person ', 'landlubber ')
	content = content.replace('Person ', 'Landlubber ')
	content = content.replace('girl ', 'lass ')
	content = content.replace('Girl ', 'Lass ')
	content = content.replace('girls ', 'lasses ')
	content = content.replace('Girls ', 'Lasses ')
	content = content.replace('Does ', 'Do ')
	content = content.replace(' does ', ' do ')
	content = content.replace(' doesn\'t', ' dern\'t')
	content = content.replace('I\'m ', 'I be ')
	content = content.replace('he\'s ', 'he be ')
	content = content.replace('He\'s ', 'He be ')
	content = content.replace('she\'s ', 'she be ')
	content = content.replace('She\'s ', 'She be ')
	content = content.replace('They\'re ', 'They be ')
	content = content.replace('We\'re ', 'We be ')
	content = content.replace('know ', 'be knowin\' ')
	content = content.replace(' men ', ' scurvy dogs ')
	content = content.replace(' man ', ' scurvy dog ')
	content = content.replace(' women ', ' wenches ')
	content = content.replace(' woman ', ' wench ')
	content = content.replace(' gets ', ' be gettin\' ')
	content = content.replace(' period ', ' bloody hell ')
	content = content.replace(' period. ', ' bloody hell. ')
	content = content.replace(' menstrual ', ' crimson tide ')
	content = content.replace(' menstruating ', ' fish gutting ')
	content = content.replace(' menstruates ', ' keel-hauls ')
	content = content.replace(' tampon ', ' clam swabber ')
	content = content.replace(' tampon. ', ' clam swabber. ')
	content = content.replace('It\'s ', 'It be ')
	content = str(content)
	return content

# Smurf Translation
def smurf(content):
	content = content.replace('fuck', 'smurf')
	content = content.replace('Fuck', 'Smurf')
	content = content.replace('shitting', 'smurfing')
	content = content.replace('shit', 'smurf')
	content = content.replace('bitch', 'smurf')
	content = content.replace('whore', 'smurf')
	content = content.replace(' ass ', 'smurf')
	content = content.replace('cunt', 'smurfina')
	content = content.replace('smut', 'smurf')
	content = content.replace('slag', 'smurf')
	content = content.replace('dick', 'smurf')
	content = content.replace('cock', 'smurf')
	content = content.replace('penis', 'smurfenis')
	content = content.replace('pussy', 'smurfina')
	content = content.replace('vagina', 'smurfina')
	content = content.replace('slit', 'smurf')
	content = content.replace('slut', 'smurf')
	content = content.replace('drug', 'smurf')
	content = content.replace('god', 'Papa Smurf')
	content = content.replace('God', 'Papa Smurf')
	content = content.replace('feces', 'smurfes')
	content = content.replace('Penis', 'Smurfenis')
	return content

# Join supplied list of channels from settings file.
def joinchan(ircsock, channels):
	for channel in channels:
		ircsock.send('JOIN ' + channel + '\n')

# Clean-up raw IRC input before handling it.
def process_message(irc_rawdata):
	global user
	global hostmask
	global channel
	global content
	info_list = irc_rawdata.split(' ')
	user_host = info_list[0]
	user_host = user_host.split('!')
	user = user_host[0].replace(':', '')
	hostmask = user_host[1].split('@')
	hostmask = hostmask[1]
	channel = info_list[2]
	content = irc_rawdata.split(' :')
	content = str(content[1])

# A quick and easy way to check for core bot commands.
def command_check(content):
	for command in commands:
		if command in content and command == '!help':
			# Check for info request
			# Check for shutdown command (not terrifically secure if someone has your name)
			speak(channel, language, 'I am Redbeard, a customizable, python-based IRC bot.')
			speak(channel, language, 'My functions are:')
			speak(channel, language, '!help: Print this output')
			speak(channel, language, '!info: Print basic bot information')
			speak(channel, language, '!joke: Tell a random, often cheesy joke')
			speak(channel, language, '!dirtyjoke: Tell a random, often offensive joke')
			speak(channel, language, '!translate <language> <text>: Translate <text> to <language>, either pirate or smurf')
			speak(channel, language, '!fortune: Get a random fortune')
			speak(channel, language, '!quote: Get a random quote')
                        speak(channel, language, '!excuse: Get a random BOFH-style excuse')
			speak(channel, language, '!shutdown: Shutdown the bot if you are the owner')
		if command in content and command == '!info':
			speak(channel, language, 'My localtime: ' + datetime.datetime.now().strftime('%A, %d. %B %Y %I:%M%p'))

		if '!translate pirate ' in content:
			content = content.replace('!translate pirate ', '')
			content = pirate(content)
			speak(channel, language, content)

		if '!translate smurf ' in content:
			content = content.replace('!translate smurf ', '')
			content = smurf(content)
			speak(channel, language, content)

		if command in content and user == owner and command == '!shutdown':
			speak(channel, language, 'Shutting down...')
			quit()


# Function for initiating a connection and providing a socket to send and recieve info.
# This creates a global socket which may allow a module to send directly and bypass all bot 
# functions, and should be more cleanly implemented.
def connect():
	try:
		sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		sock.connect((server, port))
	except:
		print 'Unable to connect.'
		quit()
	if ssl_enabled == True:
		ircsock = ssl.wrap_socket(sock)
	ircsock.send('USER ' + nick + ' ' + nick + ' ' + nick + ' :Tagline!\n')
	ircsock.send('NICK ' + nick + '\n')
	joinchan(ircsock, channels)
	return ircsock

# Main bot loop.
def main():
	pid = os.fork()
	if pid > 0:
		sys.exit(0)
	os.setsid()
	print 'Loaded. Connecting to ' + server + ' and becoming a daemon...'
	global ircsock
	ircsock = connect()
	while True:
		irc_rawdata = ircsock.recv(1024)  # Set a buffer for data obtained by the server
		if debug == 1:
			print irc_rawdata
		irc_rawdata = irc_rawdata.strip('\n\r')  # Clear unwanted linebreaks in the raw data
		if 'PING :' in irc_rawdata:
			ping()
		elif 'PRIVMSG' in irc_rawdata:
			data = process_message(irc_rawdata)
			if debug == 1:
				print data
			command_check(content)
			check_modules(nick, user, hostmask, channel, content)

# Run the script.
if __name__ == "__main__":
	main()
